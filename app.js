const express = require("express");
const bodyParser = require("body-parser");

const { config } = require("./config/app.config");
const logger = require("./app/utility/logger");
const messages = require("./app/utility/messages");

const app = express();

app.use(bodyParser.urlencoded({ limit: "1mb", extended: true }));

app.use(bodyParser.json({ limit: "1mb", extended: true }));

app.use((_req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-Width, Content-Type, Accept"
  );
  next();
});

process
  .on("uncaughtException", async err => {
    logger.error(`${messages.uncaughtException} ${err}`);
    process.exit(1);
  })
  .on("unhandledRejection", async (reason, promise) => {
    const promiseString = JSON.stringify(promise);
    logger.error(
      `Unhandled Rejection at ${promiseString} . Exiting. ${reason}`
    );
    process.exit(1);
  });

// Webhook route included here
require("./app/routes/app.routes.js")(app);

// Application bootrap logic
let server;
const startApplication = async () => {
  try {
    const port = config.get("port");
    server = app.listen(port, async () => {
      logger.info(`${messages.serverStart} ${port}`);
    });
  } catch (error) {
    logger.error(messages.serverStartFailed);
    process.exit(1);
  }
};

// SIGINT and SIGTERM signal Handlers
const signals = {
  SIGINT: 2,
  SIGTERM: 15
};

const shutdown = (signal, value) => {
  server.close(() => {
    logger.info(`Signal caught. Graceful exit..Server stopped by ${signal}`);
    process.exit(128 + value);
  });
};

Object.keys(signals).forEach(signal => {
  process.on(signal, () => {
    shutdown(signal, signals[signal]);
  });
});

// Decides whether app should run in dev/docker mode  or serverless based on SERVERLESS_DEPLOYMENT value in env.
if (config.get("serverlessDeployment")) {
  module.exports = app;
} else {
  startApplication();
}
