const fetch = require("node-fetch");

const logger = require("../utility/logger");
const { config } = require("../../config/app.config");
const messages = require("../utility/messages");

// Logic to search the number of times word occured in issue description. It counts exact words not substrings.
const countOccurance = (description, word) => {
  // const regExp = new RegExp(word, "g"); For changing the logic to substring matching enable this and remove exisitng regex.
  const regExp = new RegExp(`\\b${word}\\b`, "g");
  const count = (description.match(regExp) || []).length;
  return count;
};

// Controller for Gitlab webhook
const newIssueCreated = async (req, res) => {
  try {
    const projectDetails = req.body.project;
    const issueDetails = req.body.object_attributes;
    const searchWord = config.get("searchWord");
    const gitlabBaseURL = config.get("gitlabAPIBaseURL");
    let occuranceOfWord = countOccurance(issueDetails.description, searchWord);

    if (occuranceOfWord) {
      const getIssuesOptions = {
        method: "GET",
        headers: { "Private-Token": config.get("gitlabAccessKey") }
      };

      // Issue stats of given project. It gives total number of issues under one project
      const getIssueStats = async () => {
        const getIssuesStatsURL = `${gitlabBaseURL}${projectDetails.id}/issues_statistics?scope=all`;
        const issueStatsResposne = await fetch(
          getIssuesStatsURL,
          getIssuesOptions
        );
        const issueStats = await issueStatsResposne.json();
        return issueStats;
      };

      // Words counting and sum up happends here. It takes account of issues from a single paginated result.
      const getsearchWordCount = issues => {
        let pingCount = 0;
        issues.forEach(issue => {
          if (issue.iid !== issueDetails.iid) {
            pingCount += countOccurance(issue.description, searchWord);
          }
        });
        return pingCount;
      };

      // Post comment to newly created issue with "REPLACE_WORD #OCCURANCE OF SEARCH_WORD"
      const postComment = async () => {
        const postCommentParams = {
          method: "POST",
          headers: { "Private-Token": config.get("gitlabAccessKey") }
        };
        const replaceWord = config.get("replaceWord");
        const postCommentURL = `${gitlabBaseURL}${projectDetails.id}/issues/${issueDetails.iid}/notes?body=${replaceWord} ${occuranceOfWord}`;
        let response = await fetch(postCommentURL, postCommentParams);
        response = await response.json();
        logger.info(`${messages.success} : ${response.body}`);
        return res
          .status(200)
          .json({ message: `${messages.success} : ${response.body}` });
      };

      getIssueStats()
        .then(async issueStats => {
          const issuePaginationCount = config.get("issuePaginationCount");
          let totalIssueCount = issueStats.statistics.counts.all;
          let page = 1;
          const promises = [];
          // Pagination logic implemented. ISSUE_PAGINATION_COUNT in env can change number of items to fetch in a single call.
          while (totalIssueCount > 0) {
            const getIssuesUrl = `${gitlabBaseURL}${projectDetails.id}/issues?page=${page}&per_page=${issuePaginationCount}&scope=all`;
            promises.push(fetch(getIssuesUrl, getIssuesOptions));
            totalIssueCount -= issuePaginationCount;
            page += 1;
          }
          const responses = await Promise.all(promises);
          const paginatedIssueList = await Promise.all(
            responses.map(response => response.json())
          );
          paginatedIssueList.forEach(issues => {
            occuranceOfWord += getsearchWordCount(issues);
          });
          postComment();
        })
        .catch(err => {
          logger.exception(err);
        });
    } else {
      return res
        .status(200)
        .json({ message: `The word ${searchWord} not found` });
    }
  } catch (error) {
    logger.exception(error);
    return res.status(500).json({ message: messages.internalServerError });
  }
  return null;
};

module.exports = {
  newIssueCreated
};
