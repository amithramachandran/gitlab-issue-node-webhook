const messages = require("../utility/messages");

// Its a basic implementation of middleware. We can perform user authentication and other security logics here.
const validateRequest = (req, res, next) => {
  if (req.get("content-type") === "application/json") {
    next();
  } else {
    res.status(400).json({
      message: messages.badRequest
    });
  }
};

module.exports = {
  validateRequest
};
