const messages = Object.freeze({
  success: "webhook executed succesfully and commented on the issue",
  internalServerError: "Inter server error occured",
  badRequest: "Bad Request",
  serverStart: "Webhook server running at port",
  serverStartFailed:
    "Failed to start the service, encountered errors. Exiting!",
  uncaughtException: "Uncaught Exception. Exiting. Error:"
});

module.exports = messages;
