const { newIssueCreated } = require("../controller/webhook.controller");
const {
  validateRequest
} = require("../middleware/request.validator.middleware");

// Exposed routes listed here
module.exports = function routes(router) {
  router.route("/issue_created").post(validateRequest, newIssueCreated);
};
