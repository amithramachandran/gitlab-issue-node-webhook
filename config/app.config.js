const convict = require("convict");
const dotenv = require("dotenv");

dotenv.config();

const config = convict({
  ip: {
    doc: "The IP address of server.",
    format: "ipaddress",
    default: "127.0.0.1",
    env: "IP"
  },
  port: {
    doc: "The port to bind with server.",
    format: "port",
    default: 8080,
    env: "PORT"
  },
  searchWord: {
    doc: "The word to be search in the description.",
    default: "ping",
    env: "SEARCH_WORD"
  },
  replaceWord: {
    doc: "The word to be replaced in the description.",
    default: "pong",
    env: "REPLACE_WORD"
  },
  gitlabAccessKey: {
    doc: "The secret key for accessing gitlab apis.",
    default: "",
    env: "SECRET_KEY"
  },
  gitlabAPIBaseURL: {
    doc: "The base url for accessing gitlab apis specific to projects.",
    format: "url",
    default: "",
    env: "GITLAB_BASE_URL"
  },
  issuePaginationCount: {
    doc: "Issue list API is paginated. Default is 20",
    default: 20,
    env: "ISSUE_PAGINATION_COUNT"
  },
  serverlessDeployment: {
    doc: "make this value true when we need serverless deployment",
    default: false,
    env: "SERVERLESS_DEPLOYMENT"
  }
});

config.validate({ allowed: "strict" });

module.exports = {
  config
};
