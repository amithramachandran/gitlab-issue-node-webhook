# Table of Contents

[Introduction](#introduction)
[Pre Requisites for the service](#pre-requisites)
[Using the service for Development](#using-the-service-for-development)
[Using the service with Docker](#using-the-service-with-docker)
[Webhook installation](#webhook-installation)
[Serverless AWS Lambda](#serverless-aws-lambda)
[Nginx proxy](#nginx-proxy)

# Introduction

A Node.js web service that interacts as a webhook for Gitlab. This service will trigger an issue event(creation). It

will search in the description of the created issue for the word `SEARCH_WORD`. If it finds `SEARCH_WORD`:

1. It will search through all the issues under the project to check whether `SEARCH_WORD` present or not.

2. Count the total number of occurrence of `SEARCH_WORD`.

3. Add a comment on the newly created issue in Gitlab with `REPLACE_WORD + occurance of SEARCH_WORD`.

## Pre Requisites

1. Install dependencies `npm install`

2. Install the following plugins in Visual Studio Code for development.

   a. `ESLint`

   b. `Prettier`

3. Update `.env` file to configure the application to work as expected with a project in Gitlab.

   | Environment Variable   | Description                                              |
   | :--------------------- | :------------------------------------------------------- |
   | IP                     | IP for server.                                           |
   | PORT                   | Port for server.                                         |
   | GITLAB_BASE_URL        | Base URL to fetch issues/Post comments in Gitlab.        |
   | SEARCH_WORD            | Word to be search in issue description (default: ping)   |
   | REPLACE_WORD           | Word to be commented in new issue (default: pong)        |
   | SECRET_KEY             | Secret key to access Gitlab API's (Refer below)          |
   | ISSUE_PAGINATION_COUNT | Number of issues to fetch from single API (Default : 20) |
   | SERVERLESS_DEPLOYMENT  | Flag to enable serverless deployment (Default : false)   |

To create Gitlab access token(`SECRET_KEY`) (https://gitlab.com/profile/personal_access_tokens).

To Get Gitlab project ID with projet (`Project --> Settings --> Genaeral --> [Copy project ID from the screen]`)

## Using the service for Development

1. Update the environment variable information in the file `config/app.config.js` file with relevant info as well as

   `.env` file.

2. Start the node app

   `npm start`

3. To check lint issues, use

   `npm run lint`

## Using the service with Docker

1. Update `.env` files with required details(Refer Pre Requisites)

2. Build the image using

   `docker build --build-arg PORT=8080 -t gitlab_node_webhook .` (Use sudo, if required)

3. Start the container using

   `docker run -p 8080:8080 gitlab_node_webhook` (Use sudo, if required). This will start the server.

4. Create an issue in the project where we added webhook and comment will be added immediately if description includes
   desired SEARCH_WORD.

## Webhook installation

1. For a specific project go to (`specific project > settings > integrations`).

2. Add webhook URL on URL field. Add a secret key for additonal security in secret token field.

3. Choose required trigger events.(e.g : For issue creation choose issue events).

4. Add Webhook.

Note: The webhook IP should be a reachable public IP.

## Serverless AWS Lambda

1.  Install AWS CLI package for accessing AWS APIs through command line.

    `sudo apt-get install awscli`

2.  Configure AWS using the IAM credentials

    `aws configure`

    Enter following values:

    ```
    Access key ID :
    Secret Access key:
    Region:
    output format: json
    ```

    verify the values in `cat ~/.aws/credentials` and `cat ~/.aws/config` in linux.

3.  For the first time to generate the package and create lambda function in AWS run following commands

    `./node_modules/.bin/claudia generate-serverless-express-proxy --express-module app`

    (`app` is entry file name without `.js` extension).

    `./node_modules/.bin/claudia create --handler lambda.handler --deploy-proxy-api --region us-east-2`

    (change region value with your region)

    For generating IAM credentials please refer:

    https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html

4.  If all configurations are correct it will json response similar to following:

    ```json
    {
      "lambda": {
        "role": "user role",
        "name": "lambda name",
        "region": "us-east-2"
      },
      "api": {
        "id": "api id",
        "url": "this will be the serverless endpoint url"
      }
    }
    ```

    `api.url` will be the serverless endpoint.

5.  For any further deployment to AWS lambda from the develpment machine, run:

    `npm run serverless_deploy`

## Nginx Proxy

To avoid exposing the webhook directly outside the world, we can use nginx as a web server to proxy pass the requests to node app.

Currently we have a minimal configuration for proxy pass (Refer folder `nginx --> default`).

1. Replace the content of file `/etc/nginx/sites-available/default` on server (AWS EC2) with `nginx --> default`.

2. Check syntax `sudo nginx -t` and reload new configurations `sudo /etc/init.d/nginx reload` .

3. Instead of pointing webhook to server IP point it to nginx IP (here port 80) which will proxy pass requests to server.
